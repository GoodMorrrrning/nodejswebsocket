#!/bin/bash

# Vérifier si le conteneur existe déjà
if docker ps -a --format '{{.Names}}' | grep -Eq "^nodejsapp$"; then
  # Arrêter et supprimer le conteneur existant
  docker stop nodejsapp && docker rm nodejsapp
fi

# Vérifier si le port 49160 est utilisé
port=49160
while sudo netstat -tuln | grep -Eq "^.*:${port}.*LISTEN.*$"; do
  # Incrémenter le numéro de port
  ((port++))
done

# Construire l'image Docker
docker build -t ronaldo/node-web-app .

# Exécuter le conteneur avec le nom "nodejsapp", en exposant le port détecté pour l'application Node.js
docker run --name nodejsapp -p ${port}:8080 -d ronaldo/node-web-app

# Afficher le message avec le numéro de port utilisé
echo "Node.js a démarré sur le port : ${port}"
docker image prune -f
