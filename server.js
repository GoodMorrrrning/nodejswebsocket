import { createServer } from 'http';
import staticHandler from 'serve-handler';
import ws, { WebSocketServer } from 'ws';
import mysql from 'mysql';
import { insertMessage } from './messageTools.js';
import { setNotificationSend } from './messageTools.js';
import { getMessageNonLu } from './messageTools.js';
import './test.js';

//serve static folder
const server = createServer((req, res) => {   // (1)
    return staticHandler(req, res, { public: 'public' })
});
const wss = new WebSocketServer({ server }) // (2)
wss.on('connection', (client) => {
    //var mysql = mysql;
    var connection = mysql.createConnection({
    host     : '45.155.169.74',
    user     : 'root',
    password : 'example',
    dateStrings:true,
    database : 'hamstouille'
    });
    connection.connect();
   
    process.on('SIGINT', () => {
        console.info("Interrupted");
        process.exit(0);
      })
      
        console.log('Client connected !')
        client.on('message', (msg) => {
            console.log(typeof msg);
            var data = JSON.parse(`${msg}`); // Parsing the json string.
            console.log(data.type + data.message)
            if(data.type == "text" && data.message == "error")  {
                data.status = "error";
            }
            else{
                data.status = "delivered";
            }
            if(data.type == "text"){
                insertMessage(data.userid, data.message);
                setNotificationSend(1);
                console.log(getMessageNonLu(1))
            }

            // wss.emit('error', 'plpo');
            client.send(`${JSON.stringify(data)}`);
    })
        client.on('error', (client) => {
        
        })
})
function broadcast(msg) {       // (4)
    for (const client of wss.clients) {
        if (client.readyState === ws.OPEN) {
            client.send(msg)
        }
    }
}
server.listen(process.argv[2] || 8080, () => {
    console.log(`Listening, added sigint`);
    //console.log(wss.c);
})
