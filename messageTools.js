import mysql from 'mysql';

const connection = mysql.createConnection({
  host     : '45.155.169.74',
  user     : 'root',
  password : 'example',
  dateStrings:true,
  database : 'hamstouille'
});

connection.connect();

export function insertMessage(userid, message){
    var datetime = new Date().toISOString().slice(0, 19).replace('T', ' ');
                var insertmessage = {
                                    id_client: userid,
                                    i_type: 1,
                                    i_reponse: 1,
                                    message: message,
                                    is_view: 0,
                                    is_flag: 0,
                                    dateAdd: datetime
                }
               
                console.log("la date : "+datetime);
                connection.query('insert into chat set ?', insertmessage, function (error, results, fields) {
                    if (error) throw error;
                    // Neat!oo
                  });
}

export function insertimage(userid, message, imagepath){
  var datetime = new Date().toISOString().slice(0, 19).replace('T', ' ');
              var insertmessage = {
                                  id_client: userid,
                                  i_type: 1,
                                  i_reponse: 1,
                                  message: message,
                                  upload_images: imagepath,
                                  is_view: 0,
                                  is_flag: 0,
                                  dateAdd: datetime
              }
             
              console.log("la date : "+datetime);
              connection.query('insert into chat set ?', insertmessage, function (error, results, fields) {
                  if (error) throw error;
                  // Neat!
                });
}

export function setNotificationSend(userid){
  connection.query('UPDATE chat SET is_view = ? WHERE id = ?', [1, userid], function (error, results, fields) {
    if (error) throw error;
    // ...
  });

}

export function getMessageNonLu(userid){
  connection.query("SELECT id, message, upload_images, upload_videos, dateAdd as date_add FROM `chat` WHERE id_client = ? AND i_reponse = 1 and is_view = 0", [userid], function (error, results, fields) {
    if (error) throw error;
    return results;
  });
}
